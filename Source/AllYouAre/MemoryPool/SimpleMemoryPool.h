#pragma once

// A simple memory pool implementation.

class SimpleMemoryPool
{
	unsigned char* memoryArray; // The actual memory array - start of array.
	unsigned char* next; // Next block;

	unsigned int blockSize; // Size of a block.
	unsigned int noOfBlocks; // Number of blocks.

	unsigned int initBlocks; // Initialized blocks.
	unsigned int freeBlocks; // Free blocks.

	bool allowResize; // Do we alow resize of the pool.

public:
	SimpleMemoryPool(void);
	~SimpleMemoryPool(void);

	void Reinit(void);
	bool CreatePool(unsigned int blckSize, unsigned int noOfBlcks);
	void DestroyPool(void);

	void* Allocate(void);
	void DeAllocate(void*);

	void SetAllowResize(bool allow);

private:
	unsigned char* GetAddressFromIndex(unsigned int index);
	const unsigned int GetIndexFromAddress(const unsigned char* address);

	//bool resizeMemoryArray(void);

	SimpleMemoryPool(const SimpleMemoryPool& pool) {}
};