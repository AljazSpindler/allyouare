#include "AllYouAreStd.h"
#include "SimpleMemoryPool.h"

SimpleMemoryPool::SimpleMemoryPool(void)
{
	Reinit();
}

SimpleMemoryPool::~SimpleMemoryPool(void)
{
	DestroyPool();
}

void SimpleMemoryPool::Reinit()
{
	memoryArray = NULL;
	next = 0;

	noOfBlocks = 0;
	blockSize = 0;
	freeBlocks = 0;
	initBlocks = 0;

	allowResize = true;
}

bool SimpleMemoryPool::CreatePool(unsigned int blckSize, unsigned int noOfBlcks)
{
	if (memoryArray)
	{
		DestroyPool();
	}

	blockSize = blckSize;
	noOfBlocks = noOfBlcks;

	//memoryArray = new unsigned char[blockSize * noOfBlocks]; // Init array.
	memoryArray = (unsigned char*)malloc(blockSize * noOfBlocks);

	if (memoryArray)
	{
		next = memoryArray;
		return true;
	}

	return false;
}

void SimpleMemoryPool::DestroyPool()
{
	//SAFE_DELETE_ARRAY(memoryArray);
	for (unsigned int i = 0; i < noOfBlocks; i++)
	{
		free(&memoryArray[i]);
	}

	free(memoryArray);
}

void* SimpleMemoryPool::Allocate()
{
	if (initBlocks < noOfBlocks)
	{
		unsigned int* n = (unsigned int*)GetAddressFromIndex(initBlocks);
		*n = initBlocks + 1; // Next.
		initBlocks++;
	}

	void* ret = NULL;
	if (freeBlocks > 0)
	{
		ret = (void*)next;
		--freeBlocks;

		if (freeBlocks != 0)
		{
			next = GetAddressFromIndex(*((unsigned int*)next));
		}
		else
		{
			next = NULL;
		}
	}
	return ret;
}

void SimpleMemoryPool::DeAllocate(void* n)
{
	if (next != NULL)
	{
		(*(unsigned int*)n) = GetIndexFromAddress(next);
	}
	else
	{
		*((unsigned int*)n) = noOfBlocks;
	}
	next = (unsigned char*)n;

	freeBlocks++;
}

unsigned char* SimpleMemoryPool::GetAddressFromIndex(unsigned int i)
{
	return memoryArray + (i * blockSize);
}

const unsigned int SimpleMemoryPool::GetIndexFromAddress(const unsigned char* n)
{
	return (((unsigned int)(n - memoryArray)) / blockSize);
}

void SimpleMemoryPool::SetAllowResize(bool allow)
{
	allowResize = allow;
}