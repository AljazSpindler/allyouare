﻿#pragma once

// Entry point for the game engine -> 7.3.2017
// Here we initialize - setup the game engine.

#include "../GameLoop/Init.h"

class EventManager;
class FontManager;
class LUAManager;
class BaseUI;
class SocketManager;
class NetworkEvents;
class HumanView; 

class LogManager;
class VulkanRenderer;
class MemoryManager;
class PhysicsManager;
class AnimationManager;
class RNG;
class Window;

class AllYouAreApp
{

// Variables and Constructors
private:
	// Empty.
protected:
	// We will not use Hungarian notitation, because we already have smart intelli sense.
	HINSTANCE hInstance;

	// Flags.
	bool isWindowed;
	bool isRunning;
	bool isQuitRequested;
	bool isQuitting;
	bool isEditorActive;

	int modalDiagUp;

	RECT desktopSize;
	//Point screenSize;
	int colorDepth;

	//std::map<std::wstring, std::wstring> textResource;
	//std::map<std::wstring, std::wstring> hotKeys;

public:
	AllYouAreApp();

// Functions
private:
	void RegisterEngineEvents(void);

	Window* SpawnWindow(HINSTANCE instance, HWND hwnd, int screenW, int screenH, LPCWSTR gameTitle, VulkanRenderer* renderer);
	bool Run();

protected:
	// LPARAM is a long pointer -> signed int64, WPARAM is also a pointer -> unsigned int.

	int PumpUntilMessage(UINT msgEnd, WPARAM* wParam, LPARAM* lParam);
	int EatSpecificMesasges(UINT msgType, LPARAM lParam, WPARAM wParam);
	int EatSpecificMesasges(UINT msgType, LPARAM* lParam, WPARAM* wParam);
	void FlashMinimized();

	//virtual void RegisterGameEvents(void);

public:
	const POINT &GetScreenSize();

	// Virtuals need to be defined in actual game class.
	virtual TCHAR *GameTitle() = 0;
	//virtual TCHAR *GameDirectory() = 0;
	//virtual HICON *GameIcon() = 0;

	// Win32 handling
	HWND GetHWND();
	HINSTANCE GetInstance();
	virtual bool InitInstance(HINSTANCE instance, LPWSTR lcCMDLine, HWND hwnd = NULL, int screenW = SCREEN_WIDTH, int screenH = SCREEN_HEIGHT);
	static LRESULT CALLBACK HandleGlobalMsg(HWND hwnd, UINT uMSG, WPARAM wParam, LPARAM lParam); // MsgProcessor.

	bool HasModal();
	void ForceModalExit();

	LRESULT OnDisplayChange(int colorDepth, int width, int height);
	LRESULT OnPowerBroadcast(int event);
	LRESULT OnSysCommand(WPARAM wParam, LPARAM lParam);
	LRESULT OnClose();

	LRESULT OnAltEnter();
	//LRESULT OnNcCreate(LPCREATESTRUCT cs);

	bool LoadGameStrings(std::string lang);
	std::string GetString(std::string ID);
	//int GetHotKeyForString(std::wstring ID);
	UINT MapcharToKeyCode(const char hotKey);

	//int Modal(shared_ptr<IScreenElement> modalScreen, int defaultAnswer);

	bool isEditorRunning();

	// Rendering...


	// Singletons.
	EventManager *EventManager;
	FontManager *FontManager;
	LUAManager *LUAManager;
	BaseUI *BaseUI;
	SocketManager *SocketManager;
	NetworkEvents *NetworkEvents;
	HumanView *HumanView;

	LogManager *LogManager;
	VulkanRenderer *RenderManager;
	MemoryManager *MemoryManager;
	PhysicsManager *PhysicsManager;
	AnimationManager *AnimationManager;
	RNG *Random;

	Window *MainWindow;

	void AbortGame();
	int GetExitCode();
	bool GetIsRunning();
	void SetQuitting(bool quitting);

	//GameLogic* GetGameLogic();
};

extern AllYouAreApp *allYouAreApp;