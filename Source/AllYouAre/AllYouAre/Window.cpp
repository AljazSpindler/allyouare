#include "AllYouAreStd.h"
#include "Window.h"
#include "..\Rendering\VulkanRenderer.h"
#include "..\Rendering\VulkanShared.h"
#include <array>

Window::Window(HINSTANCE instance, HWND hwnd, int width, int height, LPCWSTR name, VulkanRenderer* renderer)
{
	win32Instance = instance;
	win32Hwnd = hwnd;
	surfaceWidth = width;
	surfaceHeight = height;
	windowName = name;

	Renderer = renderer;
	Renderer->SetSurfaceWidth(width);
	Renderer->SetSurfaceHeight(height);
	InitWindow();
	if (renderer != nullptr)
	{
		InitSurface();
		InitSwapChain();
		InitSwapChainImages();
		InitDepthStencilImage();
		InitRenderPass();
		InitFrameBuffers();
		InitSync();

		Renderer->CreateCommandPool();
		Renderer->AllocateCommandBuffer();

		CreateSemaphores();
	}
}

void Window::Close()
{
	isRunning = false;
}

bool Window::Update()
{
	UpdateWindow();
	return isRunning;
}

void Window::BeginRender()
{
	// TODO : DO error check.
	vkAcquireNextImageKHR(Renderer->GetVulkanDevice(), Renderer->GetSwapChainImpl(), UINT64_MAX, VK_NULL_HANDLE, Renderer->GetSwapChainImageAvailableImpl(), Renderer->GetActiveSwapChainImageId());
	vkWaitForFences(Renderer->GetVulkanDevice(), 1, Renderer->GetSwapChainImageAvailable(), VK_TRUE, UINT64_MAX);
	vkResetFences(Renderer->GetVulkanDevice(), 1, Renderer->GetSwapChainImageAvailable());
	vkQueueWaitIdle(Renderer->GetVulkanQueue());
}

void Window::EndRender(std::vector<VkSemaphore> waitSemaphores)
{
	VkResult presentResult = VkResult::VK_RESULT_MAX_ENUM;

	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = waitSemaphores.size();
	presentInfo.swapchainCount = 1;
	presentInfo.pImageIndices = Renderer->GetActiveSwapChainImageId();
	presentInfo.pSwapchains = Renderer->GetSwapChain();
	presentInfo.pWaitSemaphores = waitSemaphores.data();
	presentInfo.pResults = &presentResult;

	// TODO : DO error check.
	vkQueuePresentKHR(Renderer->GetVulkanQueue(), &presentInfo);
}

void Window::CreateSemaphores()
{
	VkSemaphoreCreateInfo semaphoreCreateInfo{};
	semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	Renderer->GetSemaphores()->resize(1);
	VkSemaphore temp;
	vkCreateSemaphore(Renderer->GetVulkanDevice(), &semaphoreCreateInfo, nullptr, &temp);
	Renderer->SetSemaphore(temp, 0);
}

void Window::DestroySemaphores()
{
	vkDestroySemaphore(Renderer->GetVulkanDevice(), Renderer->GetSemaphoresImpl()[0], nullptr);
}

void Window::InitSurface()
{
	InitOSSurface();

	VkBool32 WSIsupported = false;
	auto gpu = Renderer->GetVulkanPhysicalDevice();
	auto surface = Renderer->GetSurfaceKHRImpl();

	vkGetPhysicalDeviceSurfaceSupportKHR(gpu, Renderer->GetVulkanGraphicsFamilyIndex(), surface, &WSIsupported);
	if (!WSIsupported)
	{
		assert(0 && "WSI not supported");
	}

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(gpu, surface, Renderer->GetSurfaceCapabilities());
	if (Renderer->GetSurfaceCapabilities()->currentExtent.width < UINT32_MAX)
	{
		surfaceWidth = Renderer->GetSurfaceCapabilities()->currentExtent.width;
		surfaceHeight = Renderer->GetSurfaceCapabilities()->currentExtent.height;
	}

	{
		uint32_t formatCount = 0;
		vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, nullptr);
		if (formatCount == 0)
		{
			assert(0 && "Surface formats missing!");
		}
		std::vector<VkSurfaceFormatKHR> formats(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(gpu, surface, &formatCount, formats.data());
		if (formats[0].format == VK_FORMAT_UNDEFINED)
		{
			Renderer->SetSurfaceFormat(VK_FORMAT_B8G8R8A8_UNORM);
			Renderer->SetSurfaceColorSpace(VK_COLORSPACE_SRGB_NONLINEAR_KHR);
		}
		else
		{
			Renderer->SetSurfaceFormat(formats[0].format);
			Renderer->SetSurfaceColorSpace(formats[0].colorSpace);
		}
	}
}

void Window::DeInitSurface()
{
	vkDestroySurfaceKHR(Renderer->GetVulkanInstance(), Renderer->GetSurfaceKHRImpl(), nullptr);
}

void Window::InitSwapChain()
{
	if (Renderer->GetSwapChainImageCountImpl() < Renderer->GetSurfaceCapabilities()->minImageCount + 1)
	{
		Renderer->SetSwapChainImageCount(Renderer->GetSurfaceCapabilities()->minImageCount + 1);
	}

	if (Renderer->GetSwapChainImageCountImpl() > Renderer->GetSurfaceCapabilities()->maxImageCount)
	{
		Renderer->SetSwapChainImageCount(Renderer->GetSurfaceCapabilities()->maxImageCount);
	}

	VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
	{
		uint32_t presentModeCount = 0;
		vkGetPhysicalDeviceSurfacePresentModesKHR(Renderer->GetVulkanPhysicalDevice(), Renderer->GetSurfaceKHRImpl(), &presentModeCount, nullptr);
		std::vector<VkPresentModeKHR> presentModeList(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(Renderer->GetVulkanPhysicalDevice(), Renderer->GetSurfaceKHRImpl(), &presentModeCount, presentModeList.data());
		for (auto i : presentModeList)
		{
			if (i == VK_PRESENT_MODE_MAILBOX_KHR)
			{
				presentMode = i;
			}
		}
	}

	VkSwapchainCreateInfoKHR swapChainCreateInfo{};
	swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapChainCreateInfo.surface = Renderer->GetSurfaceKHRImpl();
	swapChainCreateInfo.minImageCount = Renderer->GetSwapChainImageCountImpl();
	swapChainCreateInfo.imageFormat = Renderer->GetSurfaceFormat()->format;
	swapChainCreateInfo.imageColorSpace = Renderer->GetSurfaceFormat()->colorSpace;
	swapChainCreateInfo.imageExtent.width = surfaceWidth;
	swapChainCreateInfo.imageExtent.height = surfaceHeight;
	swapChainCreateInfo.imageArrayLayers = 1;
	swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapChainCreateInfo.queueFamilyIndexCount = 0;
	swapChainCreateInfo.pQueueFamilyIndices = nullptr;
	swapChainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapChainCreateInfo.presentMode = presentMode;
	swapChainCreateInfo.clipped = VK_TRUE;
	swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	// TODO: Add error checking.
	vkCreateSwapchainKHR(Renderer->GetVulkanDevice(), &swapChainCreateInfo, nullptr, Renderer->GetSwapChain());

	vkGetSwapchainImagesKHR(Renderer->GetVulkanDevice(), Renderer->GetSwapChainImpl(), Renderer->GetSwapChainImageCount(), nullptr);
}

void Window::DeInitSwapChain()
{
	vkDestroySwapchainKHR(Renderer->GetVulkanDevice(), Renderer->GetSwapChainImpl(), nullptr);
}

void Window::InitSwapChainImages()
{
	Renderer->GetSwapChainImages()->resize(Renderer->GetSwapChainImageCountImpl());
	Renderer->GetSwapChainImageViews()->resize(Renderer->GetSwapChainImageCountImpl());

	// TODO: Added error checking.
	vkGetSwapchainImagesKHR(Renderer->GetVulkanDevice(), Renderer->GetSwapChainImpl(), Renderer->GetSwapChainImageCount(), Renderer->GetSwapChainImages()->data());

	for (uint32_t i = 0; i < Renderer->GetSwapChainImageCountImpl(); i++)
	{
		VkImageViewCreateInfo imageViewCreateInfo{};
		imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageViewCreateInfo.image = Renderer->GetSwapChainImagesImpl()[i];
		imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageViewCreateInfo.format = Renderer->GetSurfaceFormat()->format;
		imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
		imageViewCreateInfo.subresourceRange.levelCount = 1;
		imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageViewCreateInfo.subresourceRange.layerCount = 1;

		VkImageView tempView;
		vkCreateImageView(Renderer->GetVulkanDevice(), &imageViewCreateInfo, nullptr, &tempView);
		Renderer->SetSwapChainImageView(tempView, i);
	}
}

void Window::DeInitSwapChainImages()
{
	for (auto view : Renderer->GetSwapChainImageViewsImpl())
	{
		vkDestroyImageView(Renderer->GetVulkanDevice(), view, nullptr);
	}
}

void Window::InitDepthStencilImage()
{
	{
		std::vector<VkFormat> formats{
			VK_FORMAT_D32_SFLOAT_S8_UINT,
			VK_FORMAT_D24_UNORM_S8_UINT,
			VK_FORMAT_D16_UNORM_S8_UINT,
			VK_FORMAT_D32_SFLOAT,
			VK_FORMAT_D16_UNORM
		};

		for (auto f : formats)
		{
			VkFormatProperties formatProperties{};
			vkGetPhysicalDeviceFormatProperties(Renderer->GetVulkanPhysicalDevice(), f, &formatProperties);
			if (formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
			{
				Renderer->SetDepthStencilFormat(f);
				break;
			}
		}
		if (Renderer->GetDepthStencilFormatImpl() == VK_FORMAT_UNDEFINED)
		{
			assert(0 && "Missing Depth Stencil format!");
		}
		if ((Renderer->GetDepthStencilFormatImpl() == VK_FORMAT_D32_SFLOAT_S8_UINT) ||
			(Renderer->GetDepthStencilFormatImpl() == VK_FORMAT_D24_UNORM_S8_UINT) ||
			(Renderer->GetDepthStencilFormatImpl() == VK_FORMAT_D16_UNORM_S8_UINT) ||
			(Renderer->GetDepthStencilFormatImpl() == VK_FORMAT_S8_UINT))
		{
			Renderer->SetStencilAvailable(true);
		}

	}

	VkImageCreateInfo imageCreateInfo{};
	imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageCreateInfo.flags = 0;
	imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	imageCreateInfo.format = Renderer->GetDepthStencilFormatImpl();
	imageCreateInfo.extent.width = surfaceWidth;
	imageCreateInfo.extent.height = surfaceHeight;
	imageCreateInfo.extent.depth = 1;
	imageCreateInfo.mipLevels = 1;
	imageCreateInfo.arrayLayers = 1;
	imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageCreateInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageCreateInfo.queueFamilyIndexCount = VK_QUEUE_FAMILY_IGNORED;
	imageCreateInfo.pQueueFamilyIndices = nullptr;
	imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

	vkCreateImage(Renderer->GetVulkanDevice(), &imageCreateInfo, nullptr, Renderer->GetDepthStencilImage());

	VkMemoryRequirements imageMemoryRequirements{};
	vkGetImageMemoryRequirements(Renderer->GetVulkanDevice(), Renderer->GetDepthStencilImageImpl(), &imageMemoryRequirements);

	uint32_t memoryIndex = FindMemoryTypeIndex(&Renderer->GetVulkanPhysicalDeviceMemoryProperties(), &imageMemoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	VkMemoryAllocateInfo memoryAllocateInfo{};
	memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memoryAllocateInfo.allocationSize = imageMemoryRequirements.size;
	memoryAllocateInfo.memoryTypeIndex = memoryIndex;

	vkAllocateMemory(Renderer->GetVulkanDevice(), &memoryAllocateInfo, nullptr, Renderer->GetDepthStencilImageMemory());
	vkBindImageMemory(Renderer->GetVulkanDevice(), Renderer->GetDepthStencilImageImpl(), Renderer->GetDepthStencilImageMemoryImpl(), 0);

	VkImageViewCreateInfo imageViewCreateInfo{};
	imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageViewCreateInfo.image = Renderer->GetDepthStencilImageImpl();
	imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	imageViewCreateInfo.format = Renderer->GetDepthStencilFormatImpl();
	imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | (Renderer->GetStencilAvailable() ? VK_IMAGE_ASPECT_STENCIL_BIT : 0);
	imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
	imageViewCreateInfo.subresourceRange.levelCount = 1;
	imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
	imageViewCreateInfo.subresourceRange.layerCount = 1;

	vkCreateImageView(Renderer->GetVulkanDevice(), &imageViewCreateInfo, nullptr, Renderer->GetDepthStencilImageView());
}

void Window::DeInitDepthStencilImage()
{
	vkDestroyImageView(Renderer->GetVulkanDevice(), Renderer->GetDepthStencilImageViewImpl(), nullptr);
	vkFreeMemory(Renderer->GetVulkanDevice(), Renderer->GetDepthStencilImageMemoryImpl(), nullptr);
	vkDestroyImage(Renderer->GetVulkanDevice(), Renderer->GetDepthStencilImageImpl(), nullptr);
}

void Window::InitRenderPass()
{
	std::array<VkAttachmentDescription, 2> attachments{};
	attachments[0].flags = 0;
	attachments[0].format = Renderer->GetDepthStencilFormatImpl();
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	attachments[1].flags = 0;
	attachments[1].format = Renderer->GetSurfaceFormat()->format;
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference subPass0DepthStencilAttachments{};
	subPass0DepthStencilAttachments.attachment = 0;
	subPass0DepthStencilAttachments.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	std::array<VkAttachmentReference, 1> subPass0ColorAttachments{};
	subPass0ColorAttachments[0].attachment = 1;
	subPass0ColorAttachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	std::array<VkSubpassDescription, 1> subPasses{};
	subPasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subPasses[0].colorAttachmentCount = subPass0ColorAttachments.size();
	subPasses[0].pColorAttachments = subPass0ColorAttachments.data();
	subPasses[0].pDepthStencilAttachment = &subPass0DepthStencilAttachments;

	VkRenderPassCreateInfo renderPassCreateInfo{};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.attachmentCount = attachments.size();
	renderPassCreateInfo.pAttachments = attachments.data();
	renderPassCreateInfo.subpassCount = subPasses.size();
	renderPassCreateInfo.pSubpasses = subPasses.data();

	//TODO: Do error check.
	vkCreateRenderPass(Renderer->GetVulkanDevice(), &renderPassCreateInfo, nullptr, Renderer->GetRenderPass());
}

void Window::DeInitRenderPass()
{
	vkDestroyRenderPass(Renderer->GetVulkanDevice(), Renderer->GetRenderPassImpl(), nullptr);
}

void Window::InitFrameBuffers()
{
	Renderer->GetFrameBuffers()->resize(Renderer->GetSwapChainImageCountImpl());

	for (uint32_t i = 0; i < Renderer->GetSwapChainImageCountImpl(); i++)
	{
		std::array<VkImageView, 2> attachments{};
		attachments[0] = Renderer->GetDepthStencilImageViewImpl();
		attachments[1] = Renderer->GetSwapChainImageViewsImpl()[i];

		VkFramebufferCreateInfo frameBufferCreateInfo{};
		frameBufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		frameBufferCreateInfo.renderPass = Renderer->GetRenderPassImpl();
		frameBufferCreateInfo.attachmentCount = attachments.size();
		frameBufferCreateInfo.pAttachments = attachments.data();
		frameBufferCreateInfo.width = surfaceWidth;
		frameBufferCreateInfo.height = surfaceHeight;
		frameBufferCreateInfo.layers = 1;

		// TODO: Error check.
		VkFramebuffer temp;
		vkCreateFramebuffer(Renderer->GetVulkanDevice(), &frameBufferCreateInfo, nullptr, &temp);
		Renderer->SetFrameBuffer(temp, i);
	}
}

void Window::DeInitFrameBuffers()
{
	for (auto buffer : Renderer->GetFrameBuffersImpl())
	{
		vkDestroyFramebuffer(Renderer->GetVulkanDevice(), buffer, nullptr);
	}
}

void Window::InitSync()
{
	VkFenceCreateInfo fenceCreateInfo{};
	fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

	vkCreateFence(Renderer->GetVulkanDevice(), &fenceCreateInfo, nullptr, Renderer->GetSwapChainImageAvailable());
}

void Window::DeInitSync()
{
	vkDestroyFence(Renderer->GetVulkanDevice(), Renderer->GetSwapChainImageAvailableImpl(), nullptr);
}
