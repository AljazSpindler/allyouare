#pragma once

// Here we include all macros for the engine.

// Simple MemoryPool Macros.

#define GCC_SIMPLEMEMORYPOOL_DECL(__defaultNoBlocks__) \
    public: \
		static SimpleMemoryPool* sMemoryPool; \
		static void InitMemoryPool(unsigned int noBlocks = __defaultNoBlocks__); \
		static void DestroyMemoryPool(void); \
        static void* operator new(size_t size); \
        static void operator delete(void* pPtr); \
        static void* operator new[](size_t size); \
        static void operator delete[](void* pPtr); \
    private: \

#define GCC_SIMPLEMEMORYPOOL_DEF(_className_) \
	SimpleMemoryPool* _className_::sMemoryPool = NULL;\
	void _className_::InitMemoryPool(unsigned int noBlocks) \
	{ \
		if (sMemoryPool != NULL) \
		{ \
			SAFE_DELETE(sMemoryPool); \
		} \
		sMemoryPool = GCC_NEW SimpleMemoryPool; \
		sMemoryPool->CreatePool(sizeof(_className_), noBlocks); \
	} \
	void _className_::DestroyMemoryPool(void) \
	{ \
		assert(sMemoryPool != NULL); \
		SAFE_DELETE(sMemoryPool); \
	} \
    void* _className_::operator new(size_t size) \
    { \
        assert(sMemoryPool); \
        void* pMem = sMemoryPool->Allocate(); \
        return pMem; \
    } \
    void _className_::operator delete(void* pPtr) \
    { \
        assert(sMemoryPool); \
        sMemoryPool->DeAllocate(pPtr); \
    } \
    void* _className_::operator new[](size_t size) \
    { \
        assert(sMemoryPool); \
        void* pMem = sMemoryPool->Allocate(); \
        return pMem; \
    } \
    void _className_::operator delete[](void* pPtr) \
    { \
        assert(sMemoryPool); \
        sMemoryPool->DeAllocate(pPtr); \
    } \


#define GCC_SIMPLEMEMORYPOOL_AUTOINIT_DEBUGNAME(_className_, _noBlocks_) \
class _className_ ## _AutoInitializedMemoryPool \
{ \
public: \
	_className_ ## _AutoInitializedMemoryPool(void); \
	~_className_ ## _AutoInitializedMemoryPool(void); \
}; \
_className_ ## _AutoInitializedMemoryPool::_className_ ## _AutoInitializedMemoryPool(void) \
{ \
	_className_::InitMemoryPool(_noBlocks_); \
} \
_className_ ## _AutoInitializedMemoryPool::~_className_ ## _AutoInitializedMemoryPool(void) \
{ \
	_className_::DestroyMemoryPool(); \
} \
static _className_ ## _AutoInitializedMemoryPool s_ ## _className_ ## _AutoInitializedMemoryPool; \

#define GCC_SIMPLEMEMORYPOOL_AUTOINIT(_className_, _noBlocks_) GCC_SIMPLEMEMORYPOOL_AUTOINIT_DEBUGNAME(_className_, _noBlocks_)
		