#include "AllYouAreStd.h"
#include "AllYouAre.h"

INT WINAPI AllYouAreEngine(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	if (allYouAreApp != nullptr)
	{
		if (!allYouAreApp->InitInstance(hInstance, lpCmdLine, 0, 800, 600))
		{
			return -1;
		}
	}

	return 0;
}