#pragma once

class VulkanRenderer;

class Window
{
public:
	Window(HINSTANCE instance, HWND hwnd, int width, int height, LPCWSTR name, VulkanRenderer* renderer);

	HINSTANCE GetInstance() { return win32Instance; }
	HWND GetHwnd() { return win32Hwnd; }
	LPCWSTR GetHwndClassName() { return win32ClassName; }
	uint64_t GetHwndClassIdCounter() { return win32ClassIdCounter; }

	void Close();
	bool Update();
	void DeInitWindow();

private:
	VulkanRenderer* Renderer;

	void BeginRender();
	void EndRender(std::vector<VkSemaphore> waitSemaphores);

	void CreateSemaphores();
	void DestroySemaphores();

	void InitWindow();
	void UpdateWindow();
	void InitOSSurface();

	void InitSurface();
	void DeInitSurface();

	void InitSwapChain();
	void DeInitSwapChain();

	void InitSwapChainImages();
	void DeInitSwapChainImages();

	void InitDepthStencilImage();
	void DeInitDepthStencilImage();

	void InitRenderPass();
	void DeInitRenderPass();

	void InitFrameBuffers();
	void DeInitFrameBuffers();

	void InitSync();
	void DeInitSync();

	bool isRunning = true;
	int surfaceWidth = 800;
	int surfaceHeight = 600;
	LPCWSTR windowName;

#if VK_USE_PLATFORM_WIN32_KHR
	HINSTANCE win32Instance = NULL;
	HWND win32Hwnd = NULL;
	LPCWSTR win32ClassName;
	static uint64_t win32ClassIdCounter;
#endif
};
