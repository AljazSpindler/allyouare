#include "AllYouAreStd.h"
#include "Window.h"
#include "..\Rendering\VulkanRenderer.h"

#if VK_USE_PLATFORM_WIN32_KHR

uint64_t	Window::win32ClassIdCounter = 0;

void Window::InitWindow()
{
	WNDCLASSEX winClass{};
	assert(surfaceWidth > 0);
	assert(surfaceHeight > 0);

	win32Instance = GetModuleHandle(nullptr);
	win32ClassName = windowName + '_' + win32ClassIdCounter;
	win32ClassIdCounter++;

	// Initialize the window class structure:
	winClass.cbSize = sizeof(WNDCLASSEX);
	winClass.style = CS_HREDRAW | CS_VREDRAW;
	winClass.lpfnWndProc = AllYouAreApp::HandleGlobalMsg;
	winClass.cbClsExtra = 0;
	winClass.cbWndExtra = 0;
	winClass.hInstance = win32Instance; // hInstance
	winClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	winClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	winClass.lpszMenuName = NULL;
	winClass.lpszClassName = win32ClassName;
	winClass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	// Register window class:
	if (!RegisterClassEx(&winClass)) {
		// It didn't work, so try to give a useful error:
		assert(0 && "Cannot create a window in which to draw!\n");
	}
	 
	DWORD ex_style = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	DWORD style = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;

	// Create window with the registered class:
	RECT wr = { 0, 0, LONG(surfaceWidth), LONG(surfaceHeight) };
	AdjustWindowRectEx(&wr, style, FALSE, ex_style);
	win32Hwnd = CreateWindowEx(0,
		win32ClassName,		// class name
		windowName,			// app name
		style,							// window style
		CW_USEDEFAULT, CW_USEDEFAULT,	// x/y coords
		wr.right - wr.left,				// width
		wr.bottom - wr.top,				// height
		NULL,							// handle to parent
		NULL,							// handle to menu
		win32Instance,				// hInstance
		NULL);							// no extra parameters
	if (!win32Hwnd) {
		// It didn't work, so try to give a useful error:
		assert(0 && "Cannot create a window in which to draw!\n");
	}
	SetWindowLongPtr(win32Hwnd, GWLP_USERDATA, (LONG_PTR)this);

	ShowWindow(win32Hwnd, SW_SHOW);
	SetForegroundWindow(win32Hwnd);
	SetFocus(win32Hwnd);
}

void Window::DeInitWindow()
{
	if (Renderer != nullptr)
	{
		DeInitSync();
		DeInitFrameBuffers();
		DeInitRenderPass();
		DeInitDepthStencilImage();
		DeInitSwapChainImages();
		DeInitSwapChain();
		DeInitSurface();

		vkQueueWaitIdle(Renderer->GetVulkanQueue());
		DestroySemaphores();
		vkDestroyCommandPool(Renderer->GetVulkanDevice(), Renderer->GetCommandPoolImpl(), nullptr);
	}
	DestroyWindow(win32Hwnd);
	UnregisterClass(win32ClassName, win32Instance);
}

void Window::UpdateWindow()
{
	MSG msg;
	if (PeekMessage(&msg, win32Hwnd, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if (this != nullptr)
	{
		if (isRunning)
		{
			BeginRender();
			Renderer->ProcessComandBuffers();
			EndRender(Renderer->GetSemaphoresImpl());
		}
	}
}

void Window::InitOSSurface()
{
	VkWin32SurfaceCreateInfoKHR createInfo {};
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.hinstance = win32Instance;
	createInfo.hwnd = win32Hwnd;

	vkCreateWin32SurfaceKHR(Renderer->GetVulkanInstance(), &createInfo, nullptr, Renderer->GetSurfaceKHR());
}

#endif


