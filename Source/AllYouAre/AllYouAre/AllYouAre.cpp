﻿#include "AllYouAreStd.h"
#include "AllYouAre.h"

#include <fstream>
#include "../Rendering/VulkanRenderer.h"
#include "Window.h"

AllYouAreApp *allYouAreApp = NULL;

//using namespace rapidjson;
// Implement the App Code.


AllYouAreApp::AllYouAreApp()
{
	allYouAreApp = this;

	isWindowed = false;
	isRunning = false;
	isQuitRequested = false;
	isQuitting = false;
	isEditorActive = false;

	modalDiagUp = 0;

	desktopSize.left = desktopSize.right = desktopSize.bottom = desktopSize.top = 0;
	//screenSize = Point(0, 0);
	colorDepth = 32;

	EventManager = NULL;
	FontManager = NULL;
	LUAManager = NULL;
	SocketManager = NULL;
	NetworkEvents = NULL;

	LogManager = NULL;
	RenderManager = NULL;
	MemoryManager = NULL;
	PhysicsManager = NULL;
	AnimationManager = NULL;
	Random = NULL;
}

bool AllYouAreApp::InitInstance(HINSTANCE instance, LPWSTR lcCMDLine, HWND hwnd, int screenW, int screenH)
{
	// Check if this is the only windows running the game.
	if (!CheckOnlyInstance(GameTitle()))
	{
		return false;
	}

	// Check System resources. Go for minimum specs the game engine can run with.

	if(!CheckDriveSpace(100 * MB))
	{
		return false;
	}

	if (!CheckMemory(512 * MB, 512 * MB))
	{
		return false;
	}

	if (!CheckCPUSpeed(1200))
	{
		return false;
	}


	//if (!LoadGameStrings("en"))
	//{
	//	return false;
	//}

	hInstance = instance;

	// Check if we can render with Vulkan. Otherwise quit/continue in Debug.

	RenderManager = new VulkanRenderer();
	Window *w = SpawnWindow(hInstance, hwnd, screenW, screenH, GameTitle(), RenderManager);
	// Tut 12 - 36:31

	while (Run())
	{
		// CPU work.

		w->Update();
		// Update does all of the bellow functions.
		// Begin render.
		// Record command buffer.
		// Submit command buffer.
		// End render.
	}

	vkQueueWaitIdle(RenderManager->GetVulkanQueue());

	//RenderManager->DestroyCommandPool();

	return true;
}

Window * AllYouAreApp::SpawnWindow(HINSTANCE instance, HWND hwnd, int screenW, int screenH, LPCWSTR gameTitle, VulkanRenderer* renderer)
{
	MainWindow = new Window(instance, hwnd, screenW, screenH, gameTitle, renderer);
	return MainWindow;
}

bool AllYouAreApp::Run()
{
	if (MainWindow != nullptr)
	{
		return MainWindow->Update();
	}
	return true;
}

LRESULT CALLBACK AllYouAreApp::HandleGlobalMsg(HWND hwnd, UINT uMSG, WPARAM wParam, LPARAM lParam)
{
	Window * window = reinterpret_cast<Window*>(
		GetWindowLongPtrW(hwnd, GWLP_USERDATA));

	switch (uMSG) 
	{
		case WM_CLOSE:
			window->Close();
			return allYouAreApp->OnClose();
		case WM_SIZE:
			// we get here if the window has changed size, we should rebuild most
			// of our window resources before rendering to this window again.
			// ( no need for this because our window sizing by hand is disabled )
			break;
		case WM_KEYDOWN:
		case WM_KEYUP:
		case WM_CHAR:
		case WM_MOUSEMOVE:
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		{

		}
		default:
			break;
	}
	return DefWindowProc(hwnd, uMSG, wParam, lParam);
}

LRESULT AllYouAreApp::OnClose()
{
	MainWindow->DeInitWindow();
	return 0;
}

bool AllYouAreApp::LoadGameStrings(std::string lang)
{
	// We will be loading JSON docs here. One for each gameplay language.
	// Log errors.

	std::string languageFile = "C:\\Users\\Aljaz\\Documents\\allyouare\\Assets\\Strings\\" + lang + ".json"; // TODO: Make relative later.
	std::ifstream in(languageFile);
    std::string line, text;

	while (std::getline(in, line))
	{
		text += line + "\n";
	}
	const char* data = text.c_str();

	rapidjson::Document d;
	d.Parse(data);

	assert(d.IsObject());
	const rapidjson::Value& strings = d["strings"];

	assert(strings.IsArray());
	for (rapidjson::SizeType i = 0; i < strings.Size(); i++)
	{
		assert(strings[i].IsObject());
		// Process the data here and save it globally to text resources.
	}

	return true;
}

std::string AllYouAreApp::GetString(std::string ID)
{
	return "Empty";
}