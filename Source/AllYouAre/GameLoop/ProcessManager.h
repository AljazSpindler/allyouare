#pragma once

#include "Process.h"

class ProcessManager
{
	typedef std::list<StrongProcessPtr> Processes;
	Processes procs; // List of our processes.

public:
	~ProcessManager();

	unsigned int UpdateAllProcesses(unsigned long deltaMs);
	WeakProcessPtr AttachProcess(StrongProcessPtr proc);
	void AbortAllProcesses(bool now);
	unsigned int GetProcessCount() const;

private:
	void ClearAllProcess();
};