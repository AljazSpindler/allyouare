#include "AllYouAreStd.h"
#include "ProcessManager.h"

ProcessManager::~ProcessManager()
{
	ClearAllProcess();
}

// Updates all currently aligned processes in the procs list.
unsigned int ProcessManager::UpdateAllProcesses(unsigned long deltaMs)
{
	unsigned short int successProcs = 0;
	unsigned short int failProcs = 0;

	Processes::iterator i = procs.begin();
	while (i != procs.end()) // Iterate through the entire list and update them accordingly. UPDATE pattern.
	{
		StrongProcessPtr current = (*i);

		Processes::iterator tIt = i;
		++i;

		Process::State currentState = current->GetState(); // Fetch the current state of the process.

		if (currentState == Process::UNINITIALIZED)
		{
			current->OnInit(); // It is not yet alive -> we must start it.
		}

		if (currentState == Process::RUNNING)
		{
			current->OnUpdate(deltaMs); // It is running already, lets perform a tick.
		}

		if (current->isDead()) // The process already died.
		{
			if (currentState == Process::SUCCEEDED)
			{
				current->OnSuccess();
				StrongProcessPtr child = current->RemoveChild();
				if (child)
				{
					AttachProcess(child); // The parent was succesefully completed, we can now attach the next on in the chain.
				}
				else 
				{
					successProcs++;
				}
			}
			else if (currentState == Process::FAILED)
			{
				current->OnFail();
				failProcs++;
			}
			else if (currentState == Process::ABORTED)
			{
				current->OnAbort();
				failProcs++;
			}

			procs.erase(tIt); // We can remove this one, because we do not need it anymore.
		}
	}

	return ((successProcs << 16) | failProcs); // Upper 16 bits return the succ number of procs and the lower 16 bits returns the failed number of procs.
}

WeakProcessPtr ProcessManager::AttachProcess(StrongProcessPtr proc)
{
	procs.push_front(proc); // Add it to the beggining.
	return WeakProcessPtr(proc);
}

void ProcessManager::AbortAllProcesses(bool now)
{
	Processes::iterator i = procs.begin();
	while (i != procs.end()) // Iterate through the entire list and update them accordingly and abort all the procs. UPDATE pattern.
	{
		Processes::iterator tempI = i;
		++i;

		StrongProcessPtr proc = *tempI;
		if (proc->isAlive())
		{
			proc->SetState(Process::ABORTED);
			if (now)
			{
				proc->OnAbort();
				procs.erase(tempI);
			}
		}
	}
}

unsigned int ProcessManager::GetProcessCount() const
{
	return procs.size();
}

void ProcessManager::ClearAllProcess()
{
	procs.clear();
}

