#include "AllYouAreStd.h"
#include "Init.h"

bool CheckOnlyInstance(LPCTSTR appName)
{
	LPCTSTR mutexName = appName;
	HANDLE mutex = CreateMutex(NULL, FALSE, mutexName);

	switch (GetLastError())
	{
	case ERROR_SUCCESS:
		break;

	case ERROR_ALREADY_EXISTS:
		return false;

	default:
		return false;
	}

	return true;
}

bool CheckDriveSpace(const unsigned __int64 driveSpaceMinimumB)
{
	unsigned __int64 FreeBytesCaller;
	unsigned __int64 TotalBytes;
	unsigned __int64 FreeBytes;
	BOOL result;

	result = GetDiskFreeSpaceEx(NULL, (PULARGE_INTEGER)&FreeBytesCaller, (PULARGE_INTEGER)&TotalBytes, (PULARGE_INTEGER)&FreeBytes);
	if (result)
	{
		if (FreeBytes < driveSpaceMinimumB)
		{
			// TODO:  Add error log message.
			return false;
		}
	}

	return true;
}

bool CheckMemory(DWORDLONG physicalMemoryMinimumB, DWORDLONG virtualMemoryMinimumB)
{
	MEMORYSTATUSEX memory;
	memory.dwLength = sizeof(MEMORYSTATUSEX);

	GlobalMemoryStatusEx(&memory);
	// TODO: Log all memory data to log. 

	if (memory.ullAvailVirtual < virtualMemoryMinimumB)
	{
		// TODO: Log error.
		return false;
	}

	if (memory.ullTotalPhys < physicalMemoryMinimumB)
	{
		// TODO: Log error.
		return false;
	}

	return true;
}

bool CheckCPUSpeed(DWORD minimumCPUSpeed)
{
	DWORD data = sizeof(DWORD);
	DWORD cpuSpeed = 0;
	HKEY cpuRegKey;

	RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", 0, KEY_READ, &cpuRegKey);

	RegQueryValueEx(cpuRegKey, L"~MHZ", NULL, NULL, (LPBYTE)&cpuSpeed, &data);
	//assert(cpuSpeed > 0);
	// TODO: Log speed.

	if (cpuSpeed < minimumCPUSpeed)
	{
		return false;
	}
	
	return true;
}