#pragma once

class Process;

typedef shared_ptr<Process> StrongProcessPtr;
typedef weak_ptr<Process> WeakProcessPtr;

class Process
{
	friend class ProcessManager;

public:
	Process();
	virtual ~Process();

	enum State
	{
		UNINITIALIZED = 0, // Created but not yet running.
		REMOVED = 1, // Currently removed, nut running.

		RUNNING = 2, // Process is running.
		PAUSED = 3, // Process is being paused.

		SUCCEEDED = 4, // Process finished with success.
		FAILED = 5, // Process finished with failure.
		ABORTED = 6, // Process was started or not, but was in mid aborted.
	};

	void Succeed();
	void Fail();
	void Pause();
	void Resume();

	State GetState() const { return state; };
	bool isAlive() const;
	bool isDead() const;
	bool isRemoved() const;
	bool isPaused() const;

	void AttachChild(StrongProcessPtr newChild);
	StrongProcessPtr RemoveChild();
	StrongProcessPtr ReturnChild();

protected:
	virtual void OnInit();
	virtual void OnUpdate(unsigned long deltaMs) = 0;
	virtual void OnSuccess() {};
	virtual void OnFail() {};
	virtual void OnAbort() {};

private:
	State state; // The state of the current process.
	StrongProcessPtr childProcess; // A pointer to the child process if any is attached. Chaining of processes.

	void SetState(State newState) { state = newState; };
};
