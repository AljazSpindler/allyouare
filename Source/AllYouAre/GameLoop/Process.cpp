#include "AllYouAreStd.h"
#include "Process.h"

Process::Process()
{
	state = UNINITIALIZED; // All process start in this state.
}

Process::~Process()
{
	if (childProcess)
	{
		childProcess->OnAbort(); // Abort all of them in the further chain.
	}
}

void Process::Succeed()
{
	assert(state == RUNNING || state == PAUSED);
	state = SUCCEEDED;
}

void Process::Fail()
{
	assert(state == RUNNING || state == PAUSED);
	state = FAILED;
}

void Process::Pause()
{
	assert(state == RUNNING);
	state = PAUSED;
}

void Process::Resume()
{
	assert(state == PAUSED);
	state = RUNNING;
}

bool Process::isAlive() const
{
	return (state == RUNNING || state == PAUSED);
}

bool Process::isDead() const
{
	return (state == SUCCEEDED || state == FAILED || state == ABORTED);
}

bool Process::isRemoved() const
{
	return state == REMOVED;
}

bool Process::isPaused() const
{
	return state == PAUSED;
}

void Process::AttachChild(StrongProcessPtr newChild)
{
	if (childProcess)
	{
		childProcess->AttachChild(newChild);
	}
	else
	{
		childProcess = newChild;
	}
}

StrongProcessPtr Process::RemoveChild()
{
	if (childProcess)
	{
		StrongProcessPtr child = childProcess;
		childProcess.reset();

		return child;
	}

	return StrongProcessPtr();
}