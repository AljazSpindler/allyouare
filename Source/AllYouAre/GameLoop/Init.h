#pragma once

extern bool CheckOnlyInstance(LPCTSTR appName);
extern bool CheckDriveSpace(const unsigned __int64 driveSpaceMinimumB);
extern bool CheckMemory(DWORDLONG physicalMemoryMinimumB, DWORDLONG virtualMemoryMinimumB);
extern bool CheckCPUSpeed(DWORD minimumCPUSpeed);
