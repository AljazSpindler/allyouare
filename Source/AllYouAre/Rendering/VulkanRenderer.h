#pragma once

class VulkanRenderer
{
public:
	VulkanRenderer();
	~VulkanRenderer();

	void CreateCommandPool();
	void DestroyCommandPool() { vkDestroyCommandPool(device, cmdPool, nullptr); };
	VkCommandPool* GetCommandPool() { return &cmdPool; };
	VkCommandPool GetCommandPoolImpl() { return cmdPool; };

	void AllocateCommandBuffer();
	void ProcessComandBuffers();

	// Getters.
	const VkInstance GetVulkanInstance() const { return instance; };
	const VkPhysicalDevice GetVulkanPhysicalDevice() const { return gpu; };
	const VkDevice GetVulkanDevice() const { return device; };
	const VkQueue GetVulkanQueue() const { return queue; };
	const uint32_t GetVulkanGraphicsFamilyIndex() const { return graphicsFamilyIndex; };
	const VkPhysicalDeviceProperties &GetVulkanPhysicalDeviceProperties() const { return gpuProperties; };
	const VkPhysicalDeviceMemoryProperties &GetVulkanPhysicalDeviceMemoryProperties() const { return gpuMemoryProperties; };
	
	VkSurfaceKHR* GetSurfaceKHR()  { return &surface; };
	VkSurfaceKHR GetSurfaceKHRImpl() { return surface; };
	VkSurfaceCapabilitiesKHR* GetSurfaceCapabilities() { return &surfaceCapabilities; };
	VkSurfaceFormatKHR* GetSurfaceFormat() { return &surfaceFormat; };
	VkSurfaceFormatKHR GetSurfaceFormatImpl() { return surfaceFormat; };
	void SetSurfaceFormat(VkFormat format) { surfaceFormat.format = format; };
	void SetSurfaceColorSpace(VkColorSpaceKHR colorSpace) { surfaceFormat.colorSpace = colorSpace; };

	VkSwapchainKHR* GetSwapChain() { return &swapChain; };
	VkSwapchainKHR GetSwapChainImpl() { return swapChain; };

	VkFence* GetSwapChainImageAvailable() { return &swapChainImageAvailable; };
	VkFence GetSwapChainImageAvailableImpl() { return swapChainImageAvailable; };

	std::vector<VkImage>* GetSwapChainImages() { return &swapChainImages; };
	std::vector<VkImage> GetSwapChainImagesImpl() { return swapChainImages; };

	std::vector<VkImageView>* GetSwapChainImageViews() { return &swapChainImageViews; };
	std::vector<VkImageView> GetSwapChainImageViewsImpl() { return swapChainImageViews; };
	void SetSwapChainImageView(VkImageView view, int index) { swapChainImageViews[index] = view; };

	VkImage* GetDepthStencilImage() { return &depthStencilImage; };
	VkImage GetDepthStencilImageImpl() { return depthStencilImage; };
	VkDeviceMemory* GetDepthStencilImageMemory() { return &depthStencilImageMemory; };
	VkDeviceMemory GetDepthStencilImageMemoryImpl() { return depthStencilImageMemory; };

	VkImageView* GetDepthStencilImageView() { return &depthStencilImageView; };
	VkImageView GetDepthStencilImageViewImpl() { return depthStencilImageView; };

	VkFormat* GetDepthStencilFormat() { return &depthStencilFormat; };
	VkFormat GetDepthStencilFormatImpl() { return depthStencilFormat; };
	void SetDepthStencilFormat(VkFormat format) { depthStencilFormat = format; };

	bool GetStencilAvailable() { return stencilAvailable;  };
	void SetStencilAvailable(bool stencilAv) { stencilAvailable = stencilAv; };

	VkRenderPass* GetRenderPass() { return &renderPass; };
	VkRenderPass GetRenderPassImpl() { return renderPass; };

	std::vector<VkFramebuffer>* GetFrameBuffers() { return &frameBuffers; };
	std::vector<VkFramebuffer> GetFrameBuffersImpl() { return frameBuffers; };
	void SetFrameBuffer(VkFramebuffer buffer, int index) { frameBuffers[index] = buffer; };

	uint32_t* GetSwapChainImageCount() { return &swapChainImageCount; };
	uint32_t GetSwapChainImageCountImpl() { return swapChainImageCount; };
	void SetSwapChainImageCount(uint32_t cnt) { swapChainImageCount = cnt; };

	uint32_t* GetActiveSwapChainImageId() { return &activeSwapChainImageId; };
	uint32_t GetActiveSwapChainImageIdImpl() { return activeSwapChainImageId; };

	std::vector<VkSemaphore>* GetSemaphores() { return &semaphores; };
	std::vector<VkSemaphore> GetSemaphoresImpl() { return semaphores; };
	void SetSemaphore(VkSemaphore semaphore, int index) { semaphores[index] = semaphore; };

	void SetSurfaceWidth(uint32_t width) { surfaceWidth = width; };
	void SetSurfaceHeight(uint32_t height) { surfaceHeight = height; };

private:
	void SetupLayerAndExtensions();

	void InitInstance();
	void DeInitInstance();

	void InitDevice();
	void DeInitDevice();

	void SetupDebug();
	void InitDebug();
	void DeInitDebug();

	VkDevice device = VK_NULL_HANDLE;
	uint32_t graphicsFamilyIndex = 0;
	VkInstance instance = VK_NULL_HANDLE;
	VkPhysicalDevice gpu = VK_NULL_HANDLE;
	VkPhysicalDeviceProperties gpuProperties = {};
	VkPhysicalDeviceMemoryProperties gpuMemoryProperties = {};
	VkCommandPool cmdPool = VK_NULL_HANDLE;
	VkCommandBuffer cmdBuffer = VK_NULL_HANDLE;
	VkQueue queue = VK_NULL_HANDLE;

	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkSurfaceCapabilitiesKHR surfaceCapabilities = {};
	VkSurfaceFormatKHR surfaceFormat = {};

	VkSwapchainKHR swapChain = VK_NULL_HANDLE;

	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;
	VkFence swapChainImageAvailable = VK_NULL_HANDLE;

	VkImage depthStencilImage = VK_NULL_HANDLE;
	VkDeviceMemory depthStencilImageMemory = VK_NULL_HANDLE;
	VkImageView depthStencilImageView = VK_NULL_HANDLE;

	VkFormat depthStencilFormat = VK_FORMAT_UNDEFINED;
	bool stencilAvailable = false;

	VkRenderPass renderPass = VK_NULL_HANDLE;

	std::vector<VkFramebuffer> frameBuffers;

	uint32_t swapChainImageCount = 2;
	uint32_t activeSwapChainImageId = UINT32_MAX;

	std::vector<VkSemaphore> semaphores;

	// Used for Vulkan debugging.
	std::vector<const char*> instanceLayers;
	std::vector<const char*> instanceExtensions;

	VkDebugReportCallbackEXT debugReport = VK_NULL_HANDLE;
	VkDebugReportCallbackCreateInfoEXT debugCallbackCreateInfo{};

	// Ignore Device layers - they are not needed.

	std::vector<const char*> deviceExtensions;

	uint32_t surfaceWidth = 600;
	uint32_t surfaceHeight = 800;
};