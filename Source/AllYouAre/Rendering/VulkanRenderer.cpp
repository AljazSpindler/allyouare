#include "AllYouAreStd.h"
#include "VulkanRenderer.h"
#include "VulkanShared.h"
#include "Window.h"
#include <array>

#ifdef _WIN32
#include <iostream>
#include <sstream>
#endif


VulkanRenderer::VulkanRenderer()
{
	SetupLayerAndExtensions();
	SetupDebug();
	InitInstance();
	InitDebug();
	InitDevice();
}

VulkanRenderer::~VulkanRenderer()
{
	DeInitDebug();
	DeInitDevice();
	DeInitInstance();
}

void VulkanRenderer::CreateCommandPool()
{
	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = graphicsFamilyIndex;
	poolInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	ErrorCheck(vkCreateCommandPool(device, &poolInfo, VK_NULL_HANDLE, &cmdPool));
}

void VulkanRenderer::AllocateCommandBuffer()
{
	VkCommandBufferAllocateInfo cmdBufferAllocateInfo{};
	cmdBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufferAllocateInfo.commandPool = cmdPool;
	cmdBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBufferAllocateInfo.commandBufferCount = 1;

	ErrorCheck(vkAllocateCommandBuffers(device, &cmdBufferAllocateInfo, &cmdBuffer));
}

void VulkanRenderer::ProcessComandBuffers()
{
	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	ErrorCheck(vkBeginCommandBuffer(cmdBuffer, &beginInfo));
	// Recording state.

	//VkViewport viewPort{};
	//viewPort.maxDepth = 1.0f;
	//viewPort.minDepth = 0.0f;
	//viewPort.width = 512;
	//viewPort.height = 512;
	//viewPort.x = 0;
	//viewPort.y = 0;

	//vkCmdSetViewport(cmdBuffer, 0, 1, &viewPort);

	VkRect2D renderArea{};
	renderArea.offset.x = 0;
	renderArea.offset.y = 0;
	renderArea.extent.height = surfaceHeight;
	renderArea.extent.width = surfaceWidth;

	std::array<VkClearValue, 2> clearValues{};
	clearValues[0].depthStencil.depth = 0.0f;
	clearValues[0].depthStencil.stencil = 0;
	clearValues[1].color.float32[0] = 0.0f;
	clearValues[1].color.float32[1] = 0.0f;
	clearValues[1].color.float32[2] = 0.0f;
	clearValues[1].color.float32[3] = 0.0f;

	VkRenderPassBeginInfo renderPassBeginInfo{};
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.renderPass = renderPass;
	renderPassBeginInfo.framebuffer = frameBuffers[activeSwapChainImageId];
	renderPassBeginInfo.renderArea = renderArea;
	renderPassBeginInfo.clearValueCount = clearValues.size();
	renderPassBeginInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(cmdBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdEndRenderPass(cmdBuffer);

	ErrorCheck(vkEndCommandBuffer(cmdBuffer)); // Compiles the command buffer on the gpu. Executable command buffer.

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &cmdBuffer;
	submitInfo.signalSemaphoreCount = semaphores.size();
	submitInfo.pSignalSemaphores = semaphores.data();


	ErrorCheck(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE));
}

void VulkanRenderer::SetupLayerAndExtensions()
{
	instanceExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
	instanceExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
	
	deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}

void VulkanRenderer::InitInstance()
{
	VkApplicationInfo applicationInfo{};
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.apiVersion = VK_MAKE_VERSION(1, 0, 42);
	applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	applicationInfo.pApplicationName = "AllYouAre";

	VkInstanceCreateInfo instanceCreateInfo{};
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	instanceCreateInfo.enabledLayerCount = instanceLayers.size();
	instanceCreateInfo.ppEnabledLayerNames = instanceLayers.data();
	instanceCreateInfo.enabledExtensionCount = instanceExtensions.size();
	instanceCreateInfo.ppEnabledExtensionNames = instanceExtensions.data();
	instanceCreateInfo.pNext = &debugCallbackCreateInfo;

	ErrorCheck(vkCreateInstance(&instanceCreateInfo, VK_NULL_HANDLE, &instance));
}

void VulkanRenderer::DeInitInstance()
{
	vkDestroyInstance(instance, VK_NULL_HANDLE);
	instance = VK_NULL_HANDLE;
}

void VulkanRenderer::InitDevice()
{
	{
		uint32_t gpuCount = 0;
		vkEnumeratePhysicalDevices(instance, &gpuCount, VK_NULL_HANDLE);
		std::vector<VkPhysicalDevice> gpuList(gpuCount);
		vkEnumeratePhysicalDevices(instance, &gpuCount, gpuList.data());
		gpu = gpuList[0];

		vkGetPhysicalDeviceProperties(gpu, &gpuProperties);
		vkGetPhysicalDeviceMemoryProperties(gpu, &gpuMemoryProperties);
	}

	{
		uint32_t familyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(gpu, &familyCount, __nullptr);
		std::vector<VkQueueFamilyProperties> family_property_list(familyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(gpu, &familyCount, family_property_list.data());

		bool found = false;
		for (uint32_t i = 0; i < familyCount; ++i)
		{
			if (family_property_list[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				found = true;
				graphicsFamilyIndex = i;
			}
		}
		if (!found)
		{
			assert(0 && "Vulkan ERROR: Queue family supporting graphics not found.");
			std::exit(-1);
		}

	}

	{
		uint32_t layerCount = 0;
		vkEnumerateInstanceLayerProperties(&layerCount, VK_NULL_HANDLE);
		std::vector<VkLayerProperties> layerPropertyList(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, layerPropertyList.data());
	}


	float queue_priorities[]{ 1.0f };
	VkDeviceQueueCreateInfo deviceQueueCreateInfo{};
	deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	deviceQueueCreateInfo.queueFamilyIndex = graphicsFamilyIndex;
	deviceQueueCreateInfo.queueCount = 1;
	deviceQueueCreateInfo.pQueuePriorities = queue_priorities;

	VkDeviceCreateInfo deviceCreateInfo{};
	deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &deviceQueueCreateInfo;
	deviceCreateInfo.enabledExtensionCount = deviceExtensions.size();
	deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions.data();

	ErrorCheck(vkCreateDevice(gpu, &deviceCreateInfo, VK_NULL_HANDLE, &device));

	vkGetDeviceQueue(device, graphicsFamilyIndex, 0, &queue);
}

void VulkanRenderer::DeInitDevice()
{
	vkDestroyDevice(device, VK_NULL_HANDLE);
	device = VK_NULL_HANDLE;
}

#if DEBUG_VULKAN

VKAPI_ATTR VkBool32 VKAPI_CALL
VulkanDebugCallback
(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objType,
	uint64_t srcObj,
	size_t location,
	int32_t msgCode,
	const char* layerPrefix,
	const char* msg,
	void* userData
)
{
	// Log messages:
	std::ostringstream stream;
	stream << "VKDBG: ";
	if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
	{
		stream << "INFO: ";
	}
	if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
	{
		stream << "WARNING: ";
	}
	if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
	{
		stream << "PERFORMANCE: ";
	}
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
	{
		stream << "ERROR: ";
	}
	if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
	{
		stream << "DEBUG: ";
	}
	stream << "@[" << layerPrefix << "]: ";
	stream << msg << std::endl;

	std::cout << stream.str();

#ifdef _WIN32
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
	{
		MessageBoxA(NULL, stream.str().c_str(), ("VULKAN ERROR!"), 0);
	}
#endif

	return false;
}

void VulkanRenderer::SetupDebug()
{
	debugCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
	debugCallbackCreateInfo.pfnCallback = VulkanDebugCallback;
	debugCallbackCreateInfo.flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
		VK_DEBUG_REPORT_WARNING_BIT_EXT |
		VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
		VK_DEBUG_REPORT_ERROR_BIT_EXT |
		VK_DEBUG_REPORT_DEBUG_BIT_EXT |
		VK_DEBUG_REPORT_FLAG_BITS_MAX_ENUM_EXT |
		0;

	instanceLayers.push_back("VK_LAYER_GOOGLE_threading");
	instanceLayers.push_back("VK_LAYER_LUNARG_swapchain");
	instanceLayers.push_back("VK_LAYER_LUNARG_screenshot");
	instanceLayers.push_back("VK_LAYER_LUNARG_monitor");
	instanceLayers.push_back("VK_LAYER_LUNARG_object_tracker");
	instanceLayers.push_back("VK_LAYER_LUNARG_parameter_validation");

	instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
}

PFN_vkCreateDebugReportCallbackEXT fvkCreateDebugReportCallbackEXT = VK_NULL_HANDLE;
PFN_vkDestroyDebugReportCallbackEXT fvkDestroyDebugReportCallbackEXT = VK_NULL_HANDLE;

void VulkanRenderer::InitDebug()
{
	fvkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	fvkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

	if (fvkCreateDebugReportCallbackEXT == VK_NULL_HANDLE || fvkDestroyDebugReportCallbackEXT == VK_NULL_HANDLE)
	{
		assert(0 && "Vulkan ERROR: Pointers fetch error.");
		std::exit(-1);
	}

	fvkCreateDebugReportCallbackEXT(instance, &debugCallbackCreateInfo, VK_NULL_HANDLE, &debugReport);
}

void VulkanRenderer::DeInitDebug()
{
	fvkDestroyDebugReportCallbackEXT(instance, debugReport, VK_NULL_HANDLE);
	debugReport = NULL;
}

#else

void VulkanRenderer::SetupDebug() {}
void VulkanRenderer::InitDebug() {}
void VulkanRenderer::DeInitDebug() {}

#endif // VULKAN_DEBUG
