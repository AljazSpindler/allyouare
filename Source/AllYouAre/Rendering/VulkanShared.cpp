#include "AllYouAreStd.h"
#include "VulkanShared.h"

#if DEBUG_VULKAN_RUNTIME

void ErrorCheck(VkResult res)
{
	if (res < 0)
	{
		switch (res)
		{
		case VK_ERROR_OUT_OF_HOST_MEMORY:
			// Log every error here or put it to the standard output.
			break;
		case VK_ERROR_OUT_OF_DEVICE_MEMORY:
			break;
		case VK_ERROR_INITIALIZATION_FAILED:
			break;
		case VK_ERROR_DEVICE_LOST:
			break;
		case VK_ERROR_MEMORY_MAP_FAILED:
			break;
		case VK_ERROR_LAYER_NOT_PRESENT:
			break;
		case VK_ERROR_EXTENSION_NOT_PRESENT:
			break;
		case VK_ERROR_FEATURE_NOT_PRESENT:
			break;
		case VK_ERROR_INCOMPATIBLE_DRIVER:
			break;
		case VK_ERROR_TOO_MANY_OBJECTS:
			break;
		case VK_ERROR_FORMAT_NOT_SUPPORTED:
			break;
		case VK_ERROR_FRAGMENTED_POOL:
			break;
		case VK_ERROR_SURFACE_LOST_KHR:
			break;
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
			break;
		case VK_SUBOPTIMAL_KHR:
			break;
		case VK_ERROR_OUT_OF_DATE_KHR:
			break;
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
			break;
		case VK_ERROR_VALIDATION_FAILED_EXT:
			break;
		case VK_ERROR_INVALID_SHADER_NV:
			break;
		case VK_ERROR_OUT_OF_POOL_MEMORY_KHR:
			break;
		case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHX:
			break;
		default:
			break;
		}
		assert(0 && "Vulkan runtime Error!");
	}
}

uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpuMemoryProperties, const VkMemoryRequirements * memoryRequirements, const VkMemoryPropertyFlags requiredProperties)
{
	uint32_t memoryIndex = UINT32_MAX;
	for (uint32_t i = 0; i < gpuMemoryProperties->memoryTypeCount; i++)
	{
		if (memoryRequirements->memoryTypeBits & (1 << i))
		{
			if ((gpuMemoryProperties->memoryTypes[i].propertyFlags & requiredProperties) == requiredProperties)
			{
				return i;
			}
		}
	}

	assert(0 && "Could not find the right memory type index!");
	return UINT32_MAX;
}

#else

void ErrorCheck(VkResult res) {}
uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpuMemoryProperties, const VkMemoryRequirements * memoryRequirements, const VkMemoryPropertyFlags requiredProperties)
{
	uint32_t memoryIndex = UINT32_MAX;
	for (uint32_t i = 0; i < gpuMemoryProperties->memoryTypeCount; i++)
	{
		if (memoryRequirements->memoryTypeBits & (1 << i))
		{
			if ((gpuMemoryProperties->memoryTypes[i].propertyFlags & requiredProperties) == requiredProperties)
			{
				return i;
			}
		}
	}

	assert(0 && "Could not find the right memory type index!");
	return UINT32_MAX;
}

#endif