#pragma once

#include <assert.h>
#include <vulkan\vulkan.h>

void ErrorCheck(VkResult res);

uint32_t FindMemoryTypeIndex(const VkPhysicalDeviceMemoryProperties * gpuMemoryProperties, const VkMemoryRequirements * memoryRequirements, const VkMemoryPropertyFlags requiredProperties);