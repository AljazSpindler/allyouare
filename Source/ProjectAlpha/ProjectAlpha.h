#pragma once

#include "resource.h"

#include "..\AllYouAre\MemoryPool\SimpleMemoryPool.h"
#include "..\AllYouAre\AllYouAre\Macros.h"


class ProjectAlphaApp : public AllYouAreApp
{
public:
	virtual TCHAR *GameTitle() { return _T("ProjectAlpha"); }

private:
	// Test
	//GCC_SIMPLEMEMORYPOOL_DECL(0);
};