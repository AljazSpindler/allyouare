// ProjectAlpha.cpp : Defines the entry point for the application.

#include "ProjectAlphaStd.h"
#include "ProjectAlpha.h"

#include "../AllYouAre/AllYouAre/AllYouAre.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

ProjectAlphaApp projectAlphaApp;

// Test
//GCC_SIMPLEMEMORYPOOL_DEF(ProjectAlphaApp);
//GCC_SIMPLEMEMORYPOOL_AUTOINIT(ProjectAlphaApp, 2);

int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{	
	return AllYouAreEngine(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
